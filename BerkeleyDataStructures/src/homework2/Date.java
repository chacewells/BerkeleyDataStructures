package homework2;

/* Date.java */

import java.util.Arrays;
import java.util.Collection;

class Date {

	/* Put your private data fields here. */
	
	private int month, day, year;

	private static enum Month {
		JANUARY(1), FEBRUARY(2), MARCH(3), APRIL(4),
		MAY(5),JUNE(6),JULY(7),AUGUST(8),
		SEPTEMBER(9),OCTOBER(10),NOVEMBER(11),DECEMBER(12);
		private int value;
		private Month(int monthNum) {
			value = monthNum;
		}
	}

	private static final Collection<Integer> SHORT_MONTHS = 
			Arrays.asList(Month.APRIL.value, Month.JUNE.value, 
					Month.SEPTEMBER.value, Month.NOVEMBER.value);

	/** Constructs a date with the given month, day and year.   If the date is
	 *  not valid, the entire program will halt with an error message.
	 *  @param month is a month, numbered in the range 1...12.
	 *  @param day is between 1 and the number of days in the given month.
	 *  @param year is the year in question, with no digits omitted.
	 */
	public Date(int month, int day, int year) {
		this("" + month + '/' + day + '/' + year);
	}

	/** Constructs a Date object corresponding to the given string.
	 *  @param s should be a string of the form "month/day/year" where month must
	 *  be one or two digits, day must be one or two digits, and year must be
	 *  between 1 and 4 digits.  If s does not match these requirements or is not
	 *  a valid date, the program halts with an error message.
	 */
	public Date(String s) {
//		ensure the date matches the pattern M(M)/D(D)/(YYY)Y
		if ( !s.matches("\\d{1,2}/\\d{1,2}/\\d{1,4}") ) {
			System.out.println("Invalid date provided: '" + s + "'");
			System.exit(1);
		}
		
		String[] monthDayYear = s.split("/", 3);
		int month = Integer.parseInt(monthDayYear[0]);
		int day = Integer.parseInt(monthDayYear[1]);
		int year = Integer.parseInt(monthDayYear[2]);
		
		if ( !isValidDate(month, day, year) ) {
			System.out.println("Invalid date provided: '" + s + "'");
			System.exit(1);
		}
		
		this.month = month;
		this.day = day;
		this.year = year;
	}

	/** Checks whether the given year is a leap year.
	 *  @return true if and only if the input year is a leap year.
	 */
	public static boolean isLeapYear(int year) {
//		order of execution important
		if (year % 400 == 0) return true;
		if (year % 100 == 0) return false;
		if (year % 4 == 0) return true;
		return false;
	}

	/** Returns the number of days in a given month.
	 *  @param month is a month, numbered in the range 1...12.
	 *  @param year is the year in question, with no digits omitted.
	 *  @return the number of days in the given month.
	 *  @throws IllegalArgumentException if the month exceeds the acceptable month
	 *  range
	 */
	public static int daysInMonth(int month, int year) {
		if (month < 1 ||  month > 12)
			throw new IllegalArgumentException("Month given is not valid.");
		if (month == Month.FEBRUARY.value)
			return isLeapYear(year) ? 29 : 28;
		else
			return (SHORT_MONTHS.contains(month)) ? 30 : 31;
	}

	/** Checks whether the given date is valid.
	 *  @return true if and only if month/day/year constitute a valid date.
	 *
	 *  Years prior to A.D. 1 are NOT valid.
	 */
	public static boolean isValidDate(int month, int day, int year) {
		boolean result = true;

		if (month < 1 || month > 12)
			return false;

		result = result &&
				day <= daysInMonth(month, year) && 
				day > 0 && 
				year > 0 && 
				(month >= 1 && month <= 12);

		return result;
	}

	/** Returns a string representation of this date in the form month/day/year.
	 *  The month, day, and year are expressed in full as integers; for example,
	 *  12/7/2006 or 3/21/407.
	 *  @return a String representation of this date.
	 */
	public String toString() {
		return "" + month + '/' + day + '/' + year;
	}

	/** Determines whether this Date is before the Date d.
	 *  @return true if and only if this Date is before d. 
	 */
	public boolean isBefore(Date d) {
		if (this.year < d.year)
			return true;
		else if (this.year > d.year)
			return false;
		else if (this.month < d.month)
			return true;
		else if (this.month > d.month)
			return false;
		else if (this.day < d.day)
			return true;
		else
			return false;
	}

	/** Determines whether this Date is after the Date d.
	 *  @return true if and only if this Date is after d. 
	 */
	public boolean isAfter(Date d) {
		return !isBefore(d) && (this.year > d.year || this.month > d.month || this.day > d.day);
	}

	/** Returns the number of this Date in the year.
	 *  @return a number n in the range 1...366, inclusive, such that this Date
	 *  is the nth day of its year.  (366 is only used for December 31 in a leap
	 *  year.)
	 */
	public int dayInYear() {
		int total = 0;
		for ( int i = 1; i < month; i++)
			total += daysInMonth(i, year);
		return total + day;
	}

	/** Determines the difference in days between d and this Date.  For example,
	 *  if this Date is 12/15/1997 and d is 12/14/1997, the difference is 1.
	 *  If this Date occurs before d, the result is negative.
	 *  @return the difference in days between d and this date.
	 */
	public int difference(Date d) {
		int difference = 0;
		Date upper = isAfter(d) ? this : d;
		Date lower = upper == this ? d : this;
		
		for (int y = lower.year; y < upper.year; y++)
			difference += isLeapYear(y) ? 366 : 365;
		
		difference += upper.dayInYear() - lower.dayInYear();
		
		difference *= isAfter(d) ? 1 : -1;
		
		return difference;
	}

	public static void main(String[] argv) {
		System.out.println("\nTesting constructors.");
		Date d1 = new Date(1, 1, 1);
		System.out.println("Date should be 1/1/1: " + d1);
		d1 = new Date("2/4/2");
		System.out.println("Date should be 2/4/2: " + d1);
		d1 = new Date("2/29/2000");
		System.out.println("Date should be 2/29/2000: " + d1);
		d1 = new Date("2/29/1904");
		System.out.println("Date should be 2/29/1904: " + d1);

		d1 = new Date(12, 31, 1975);
		System.out.println("Date should be 12/31/1975: " + d1);
		Date d2 = new Date("1/1/1976");
		System.out.println("Date should be 1/1/1976: " + d2);
		Date d3 = new Date("1/2/1976");
		System.out.println("Date should be 1/2/1976: " + d3);

		Date d4 = new Date("2/27/1977");
		Date d5 = new Date("8/31/2110");

		/* I recommend you write code to test the isLeapYear function! */

		System.out.println("\nTesting before and after.");
		System.out.println(d2 + " after " + d1 + " should be true: " + 
				d2.isAfter(d1));
		System.out.println(d3 + " after " + d2 + " should be true: " + 
				d3.isAfter(d2));
		System.out.println(d1 + " after " + d1 + " should be false: " + 
				d1.isAfter(d1));
		System.out.println(d1 + " after " + d2 + " should be false: " + 
				d1.isAfter(d2));
		System.out.println(d2 + " after " + d3 + " should be false: " + 
				d2.isAfter(d3));

		System.out.println(d1 + " before " + d2 + " should be true: " + 
				d1.isBefore(d2));
		System.out.println(d2 + " before " + d3 + " should be true: " + 
				d2.isBefore(d3));
		System.out.println(d1 + " before " + d1 + " should be false: " + 
				d1.isBefore(d1));
		System.out.println(d2 + " before " + d1 + " should be false: " + 
				d2.isBefore(d1));
		System.out.println(d3 + " before " + d2 + " should be false: " + 
				d3.isBefore(d2));

		System.out.println("\nTesting difference.");
		System.out.println(d1 + " - " + d1  + " should be 0: " + 
				d1.difference(d1));
		System.out.println(d2 + " - " + d1  + " should be 1: " + 
				d2.difference(d1));
		System.out.println(d3 + " - " + d1  + " should be 2: " + 
				d3.difference(d1));
		System.out.println(d3 + " - " + d4  + " should be -422: " + 
				d3.difference(d4));
		System.out.println(d5 + " - " + d4  + " should be 48762: " + 
				d5.difference(d4));
	}
}
