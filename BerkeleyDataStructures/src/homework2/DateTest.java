package homework2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DateTest {
//	adding a change to keep git happy
	@Test
	public void isLeapYear1900() {
		int year = 1900;
		Boolean expected = false;
		assertEquals(expected, Date.isLeapYear(year));
	}
	
	@Test
	public void isLeapYear2000() {
		int year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isLeapYear(year));
	}
	
	@Test
	public void isLeapYear1904() {
		int year = 1904;
		Boolean expected = true;
		assertEquals(expected, Date.isLeapYear(year));
	}
	
	@Test
	public void isLeapYear1991() {
		int year = 1991;
		Boolean expected = false;
		assertEquals(expected, Date.isLeapYear(year));
	}
	
	@Test
	public void daysInMonthJanuary() {
		int year = 2000,
				month = 1;
		Integer expected = 31;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthFebruary2000() {
		int year = 2000,
				month = 2;
		Integer expected = 29;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthFebruary1999() {
		int year = 1999,
				month = 2;
		Integer expected = 28;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthFebruary1900() {
		int year = 1900,
				month = 2;
		Integer expected = 28;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthFebruary1904() {
		int year = 1904,
				month = 2;
		Integer expected = 29;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthMarch() {
		int year = 2000,
				month = 3;
		Integer expected = 31;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthApril() {
		int year = 2000,
				month = 4;
		Integer expected = 30;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthMay() {
		int year = 2000,
				month = 5;
		Integer expected = 31;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthJune() {
		int year = 2000,
				month = 6;
		Integer expected = 30;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthJuly() {
		int year = 2000,
				month = 7;
		Integer expected = 31;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthAugust() {
		int year = 2000,
				month = 8;
		Integer expected = 31;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthSeptember() {
		int year = 2000,
				month = 9;
		Integer expected = 30;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthOctober() {
		int year = 2000,
				month = 10;
		Integer expected = 31;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthNovember() {
		int year = 2000,
				month = 11;
		Integer expected = 30;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test
	public void daysInMonthDecember() {
		int year = 2000,
				month = 12;
		Integer expected = 31;
		assertEquals(expected, (Integer)Date.daysInMonth(month, year));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void daysInMonth0() {
		Date.daysInMonth(0, 2000);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void daysInMonth13() {
		Date.daysInMonth(13, 2000);
	}
	
	@Test
	public void isValidDateJanuary_31_2000() {
		int month = 1, day = 31, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateJanuary_32_2000() {
		int month = 1, day = 32, year = 2000;
		Boolean expected = false;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateFebruary_29_2000() {
		int month = 2, day = 29, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateFebruary_30_2000() {
		int month = 2, day = 30, year = 2000;
		Boolean expected = false;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateFebruary_28_1999() {
		int month = 2, day = 28, year = 1999;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateFebruary_29_1999() {
		int month = 2, day = 29, year = 1999;
		Boolean expected = false;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateFebruary_29_1900() {
		int month = 2, day = 29, year = 1999;
		Boolean expected = false;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateMarch_31_2000() {
		int month = 3, day = 31, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateApril_30_2000() {
		int month = 3, day = 30, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateMay_31_2000() {
		int month = 5, day = 31, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateJune_30_2000() {
		int month = 6, day = 30, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateJuly_31_2000() {
		int month = 7, day = 31, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateAugust_31_2000() {
		int month = 8, day = 31, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateSeptember_30_2000() {
		int month = 9, day = 30, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateOctober_31_2000() {
		int month = 10, day = 31, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateNovember_30_2000() {
		int month = 11, day = 30, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateDecember_31_2000() {
		int month = 12, day = 31, year = 2000;
		Boolean expected = true;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDateDecember_31_0() {
		int month = 12, day = 31, year = 0;
		Boolean expected = false;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDate13_15_2000() {
		int month = 13, day = 15, year = 2000;
		Boolean expected = false;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void isValidDate0_15_0() {
		int month = 0, day = 15, year = 2000;
		Boolean expected = false;
		assertEquals(expected, Date.isValidDate(month, day, year));
	}
	
	@Test
	public void DayInYearDecember_31_2000() {
		int month = 12, day = 31, year = 2000;
		int expected = 366;
		assertEquals(expected, new Date(month, day, year).dayInYear());
	}
	
	@Test
	public void DayInYearDecember_31_1999() {
		int month = 12, day = 31, year = 1999;
		int expected = 365;
		assertEquals(expected, new Date(month, day, year).dayInYear());
	}
	
}
