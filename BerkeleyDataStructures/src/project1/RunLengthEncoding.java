package project1;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* RunLengthEncoding.java */

/**
 * The RunLengthEncoding class defines an object that run-length encodes an
 * Ocean object. Descriptions of the methods you must implement appear below.
 * They include constructors of the form
 *
 * public RunLengthEncoding(int i, int j, int starveTime); public
 * RunLengthEncoding(int i, int j, int starveTime, int[] runTypes, int[]
 * runLengths) { public RunLengthEncoding(Ocean ocean) {
 *
 * that create a run-length encoding of an Ocean having width i and height j, in
 * which sharks starve after starveTime timesteps.
 *
 * The first constructor creates a run-length encoding of an Ocean in which
 * every cell is empty. The second constructor creates a run-length encoding for
 * which the runs are provided as parameters. The third constructor converts an
 * Ocean object into a run-length encoding of that object.
 *
 * See the README file accompanying this project for additional details.
 */

public class RunLengthEncoding {

	/**
	 * Define any variables associated with a RunLengthEncoding object here.
	 * These variables MUST be private.
	 */
	private final int columns;
	private final int rows;
	private final int cells;
	private final int starveTime;
	private final SimpleDList<String> encoding;
	private SimpleDList.SimpleDNode<String> currentRun;
	/**
	 * The following methods are required for Part II.
	 */

	/**
	 * RunLengthEncoding() (with three parameters) is a constructor that creates
	 * a run-length encoding of an empty ocean having width i and height j, in
	 * which sharks starve after starveTime timesteps.
	 * 
	 * @param i is the width of the ocean.
	 * @param j is the height of the ocean.
	 * @param starveTime is the number of timesteps sharks survive without food.
	 */

	public RunLengthEncoding(int i, int j, int starveTime) {
		this(i, j, starveTime, new int[]{Ocean.EMPTY}, new int[]{i*j});
	}

	/**
	 * RunLengthEncoding() (with five parameters) is a constructor that creates
	 * a run-length encoding of an ocean having width i and height j, in which
	 * sharks starve after starveTime timesteps. The runs of the run-length
	 * encoding are taken from two input arrays. Run i has length runLengths[i]
	 * and species runTypes[i].
	 * 
	 * @param i is the width of the ocean.
	 * @param j is the height of the ocean.
	 * @param starveTime is the number of timesteps sharks survive without food.
	 * @param runTypes
	 *            is an array that represents the species represented by each
	 *            run. Each element of runTypes is Ocean.EMPTY, Ocean.FISH, or
	 *            Ocean.SHARK. Any run of sharks is treated as a run of newborn
	 *            sharks (which are equivalent to sharks that have just eaten).
	 * @param runLengths
	 *            is an array that represents the length of each run. The sum of
	 *            all elements of the runLengths array should be i * j.
	 */

	public RunLengthEncoding(int i, int j, int starveTime, int[] runTypes,
			int[] runLengths) {
		columns = i;
		rows = j;
		cells = columns * rows;
		this.starveTime = starveTime;
		encoding = encodeRuns(runTypes, runLengths);
	}

	/**
	 * restartRuns() and nextRun() are two methods that work together to return
	 * all the runs in the run-length encoding, one by one. Each time nextRun()
	 * is invoked, it returns a different run (represented as a TypeAndSize
	 * object), until every run has been returned. The first time nextRun() is
	 * invoked, it returns the first run in the encoding, which contains cell
	 * (0, 0). After every run has been returned, nextRun() returns null, which
	 * lets the calling program know that there are no more runs in the
	 * encoding.
	 *
	 * The restartRuns() method resets the enumeration, so that nextRun() will
	 * once again enumerate all the runs as if nextRun() were being invoked for
	 * the first time.
	 *
	 * (Note: Don't worry about what might happen if nextRun() is interleaved
	 * with addFish() or addShark(); it won't happen.)
	 */

	/**
	 * restartRuns() resets the enumeration as described above, so that
	 * nextRun() will enumerate all the runs from the beginning.
	 */

	public void restartRuns() {
		currentRun = encoding.head.next;
	}

	/**
	 * nextRun() returns the next run in the enumeration, as described above. If
	 * the runs have been exhausted, it returns null. The return value is a
	 * TypeAndSize object, which is nothing more than a way to return two
	 * integers at once.
	 * 
	 * @return the next run in the enumeration, represented by a TypeAndSize
	 *         object.
	 */

	public TypeAndSize nextRun() {
		// Replace the following line with your solution.
		String run = advanceRunPointer();
		
		return toTypeAndSize(run);
	}
	
//	nextRun() helpers
	private String advanceRunPointer() {
		String result = currentRun.data;
		if (currentRun != encoding.head) {
			currentRun = currentRun.next;
		}
		
		return result;
	}
	
	private static TypeAndSize toTypeAndSize(String run) {
		TypeAndSize result = null;
		
		if (run == null) {
			return null;
		}
		
		try {
			switch (run.charAt(0)) {
			case 'F':
				result = new TypeAndSize(Ocean.FISH,
						Integer.parseInt(run.substring(1)));
				break;
			case 'S':
				result = new TypeAndSize(Ocean.SHARK, 
						Integer.parseInt(run.substring(
								run.indexOf(',') + 1)));
				break;
			case '.':
				result = new TypeAndSize(Ocean.EMPTY, 
						Integer.parseInt(run.substring(1)));
				break;
			default:
				throw new IllegalArgumentException();
			}
		} catch (IndexOutOfBoundsException|IllegalArgumentException e) {
			result = new TypeAndSize(-1, -1);
		}
		
		return result;
	}

	/**
	 * toOcean() converts a run-length encoding of an ocean into an Ocean
	 * object. You will need to implement the three-parameter addShark method in
	 * the Ocean class for this method's use.
	 * 
	 * @return the Ocean represented by a run-length encoding.
	 */

	public Ocean toOcean() {
		// Replace the following line with your solution.
		Ocean sea = new Ocean(columns, rows, starveTime);
		SimpleDList.SimpleDNode<String> node = encoding.head;
		for (int x = 0; x < cells;) {
			node = node.next;
			String run = node.data;
			TypeAndSize typeSize = toTypeAndSize(run);
			
			for (int inx = 0; inx < typeSize.size; ++inx, ++x) {
				int y = x / columns;
				switch (typeSize.type) {
				case Ocean.SHARK:
					sea.addShark(x, y, getFeeding(run));
					break;
				case Ocean.FISH:
					sea.addFish(x, y);
					break;
				default:
					break;
				}
			}
		}
		
		return sea;
	}
	
	private static int getFeeding(String run) {
		if (toTypeAndSize(run).type != Ocean.SHARK) {
			return -1;
		}
//		get the int value between the 'S' and the ','
		return Integer.parseInt(run.substring(1, run.indexOf(',')));
	}

	/**
	 * The following method is required for Part III.
	 */

	/**
	 * RunLengthEncoding() (with one parameter) is a constructor that creates a
	 * run-length encoding of an input Ocean. You will need to implement the
	 * sharkFeeding method in the Ocean class for this constructor's use.
	 * 
	 * @param sea
	 *            is the ocean to encode.
	 */

	public RunLengthEncoding(Ocean sea) {
		this(
				sea.width(), 
				sea.height(), 
				sea.starveTime(), 
				new int[]{}, 
				new int[]{});
		
		TypeAndSize last = 
				new TypeAndSize(sea.cellContents(0, 0), 1);
		int lastFeeding = sea.sharkFeeding(0, 0);
		
		for (int x = 1; x < cells; ++x) {
			int y = x / columns;
			int type = sea.cellContents(x, y);
			int feeding = sea.sharkFeeding(x, y);
			
//			add last run to encoding
//			start a new run if different run type encountered
			if ( !(last.type == type && feeding == lastFeeding) ) {
				encoding.addLast(encodeRun(last.type, last.size, lastFeeding));
				last = new TypeAndSize(type, 1);
				lastFeeding = feeding;
				continue;
			}
			
			++last.size;
		}
//		add final run to encoding
		encoding.addLast(encodeRun(last.type, last.size, lastFeeding));
		
		// Your solution here, but you should probably leave the following line
		// at the end.
		check();
	}
	

	private int getPosX(int x) {
		return getAbsPosition(x, columns);
	}
	
	private int getPosY(int y) {
		return getAbsPosition(y, rows);
	}
	
	private static int getAbsPosition(int coord, int size) {
		if (coord < 0) {
			return Math.abs( (coord % size) + size) % size;
		}
		
		return coord % size;
	}
	
	private int getElementPosition(int x, int y) {
		int posX = getPosX(x), posY = getPosY(y);
		
		return posY * columns + posX;
	}
	
	/**
	 * The following methods are required for Part IV.
	 */

	/**
	 * addFish() places a fish in cell (x, y) if the cell is empty. If the cell
	 * is already occupied, leave the cell as it is. The final run-length
	 * encoding should be compressed as much as possible; there should not be
	 * two consecutive runs of sharks with the same degree of hunger.
	 * 
	 * @param x
	 *            is the x-coordinate of the cell to place a fish in.
	 * @param y
	 *            is the y-coordinate of the cell to place a fish in.
	 */

	public void addFish(int x, int y) {
		int elementPos = getElementPosition(x, y);
		
		// Your solution here, but you should probably leave the following line
		// at the end.
		check();
	}

	/**
	 * addShark() (with two parameters) places a newborn shark in cell (x, y) if
	 * the cell is empty. A "newborn" shark is equivalent to a shark that has
	 * just eaten. If the cell is already occupied, leave the cell as it is. The
	 * final run-length encoding should be compressed as much as possible; there
	 * should not be two consecutive runs of sharks with the same degree of
	 * hunger.
	 * 
	 * @param x
	 *            is the x-coordinate of the cell to place a shark in.
	 * @param y
	 *            is the y-coordinate of the cell to place a shark in.
	 */

	public void addShark(int x, int y) {
		// Your solution here, but you should probably leave the following line
		// at the end.
		check();
	}

	/**
	 * check() walks through the run-length encoding and prints an error message
	 * if two consecutive runs have the same contents, or if the sum of all run
	 * lengths does not equal the number of cells in the ocean.
	 */

	public void check() {
	}

	private static class SimpleDList<T> {
		SimpleDNode<T> head;
		private int size;

		public SimpleDList() {
			head = new SimpleDNode<>();
			head.next = head.prev = head;
		}

		public SimpleDList(T data) {
			head = new SimpleDNode<>();
			SimpleDNode<T> node = new SimpleDNode<>(data);
			head.next = head.prev = node;
			node.next = node.prev = head;
			++size;
		}

		public void addFirst(T data) {
			SimpleDNode<T> node = new SimpleDNode<>(data);
			node.next = head.next;
			node.prev = head;
			node.next.prev = node;
			head.next = node;
			++size;
		}

		public T removeFirst() {
			if (size < 1) {
				return null;
			}
			T result = head.next.data;
			head.next = head.next.next;
			head.next.prev = head;
			--size;

			return result;
		}

		public void addLast(T data) {
			SimpleDNode<T> node = new SimpleDNode<>(data);
			node.prev = head.prev;
			node.next = head;
			node.prev.next = node;
			head.prev = node;
			++size;
		}

		public void addAt(int position, T data) {
			if (position >= size) {
				addLast(data);
				return;
			}

			SimpleDNode<T> current = head.next;
			for (int count = 0; count < position && current.next != head; ++count, current = current.next)
				;

			SimpleDNode<T> node = new SimpleDNode<>(data);
			node.prev = current.prev;
			node.next = current;
			node.prev.next = node;
			node.next.prev = node;
			++size;
		}

		public T removeLast() {
			if (size < 1) {
				return null;
			}
			T result = head.prev.data;
			head.prev = head.prev.prev;
			head.prev.next = head;
			--size;

			return result;
		}

		public int size() {
			return size;
		}
		
		static class SimpleDNode<U> {
			U data;
			SimpleDNode<U> next;
			SimpleDNode<U> prev;

			SimpleDNode() {
				data = null;
				next = prev = null;
			}

			SimpleDNode(U data) {
				this.data = data;
				next = prev = null;
			}
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();

			sb.append("(" + size + ")");
			sb.append("[");
			for (SimpleDNode<T> node = head.next; node != head; node = node.next) {
				sb.append(" " + node.data + (node.next != head ? " |" : " "));
			}
			sb.append("]");

			return sb.toString();
		}
	}

	public static void main(String[] args) {
//		testList();
//		testRuns();
//		testConstructorNoRuns();
//		testNextRun();
//		testNextRunWithRestart();
//		testToOceanEmpty();
//		testToOcean5Empty5Sharks5Fish();
//		testOceanConstructorToOcean();
//		testAddFish_0_0();
//		testAddFish_4_4();
//		testAddFish_1_1();
//		testAddFish_5_0();
//		testAddFish_0_4();
//		testAddFish_0_0_nextToFishRun();
//		testAddFish_4_4_nextToFishRun();
//		testAddFish_1_1_alreadyAFish();
		testGetElementPosition();
	}

	private static void testList() {
		SimpleDList<String> list = new SimpleDList<>("Hello");
		list.addFirst("world");
		list.addFirst("today");
		list.addLast("tomorrow");
		System.out.println(list);

		int i = 1;
		list.addAt(i, "forever");
		System.out.println("added 'forever' to position 1");
		System.out.println(list + "\n");

		list.addAt(i, "temporary");
		System.out.println("added 'temporary' to position 1");
		System.out.println(list + "\n");

		i = 4;
		list.addAt(i, "permanent");
		System.out.println("added 'permanent' to position 4");
		System.out.println(list + "\n");

		list.addAt(7, "next day");
		System.out.println("added 'next day' to position 7");
		System.out.println(list + "\n");

		list.addAt(6, "today");
		System.out.println("added 'today' to position 6");
		System.out.println(list + "\n");

		String removed = list.removeFirst();
		System.out.println("removed '" + removed + "'");
		System.out.println(list + "\n");

		removed = list.removeLast();
		System.out.println("removed '" + removed + "'");
		System.out.println(list + "\n");

		removed = list.removeLast();
		System.out.println("removed '" + removed + "'");
		removed = list.removeLast();
		System.out.println("removed '" + removed + "'");
		System.out.println(list + "\n");
	}
	
	private static void testRuns() {
		int[] runs = {Ocean.SHARK,Ocean.FISH,Ocean.SHARK,Ocean.EMPTY,Ocean.FISH};
		int[] runLengths = {5,2,4,3,5};
		
		RunLengthEncoding rle = new RunLengthEncoding(5, 5, 2, runs, runLengths);
		System.out.println(rle.encodeRuns(runs, runLengths));
		System.out.println(rle.numCells());
		System.out.println();
	}
	
	private static void testConstructorNoRuns() {
		RunLengthEncoding rle = new RunLengthEncoding(5, 5, 15); 
		System.out.println(rle.encoding);
		System.out.println(rle.numCells());
		System.out.println();
	}
	
	private static void testNextRun() {
		Function<Integer, String> typeStr = (i) -> {
			switch (i) {
			case Ocean.EMPTY:return "EMPTY";
			case Ocean.FISH:return "FISH";
			case Ocean.SHARK:return "SHARK";
			default: return "";
			}
		};
		
		Function<TypeAndSize, String> toString =
				(t) -> "TypeAndSize[type=" +
						typeStr.apply(t.type) + 
						", size=" +
						t.size + "]";
		int[] runs = {Ocean.SHARK,Ocean.FISH,Ocean.SHARK,Ocean.EMPTY,Ocean.FISH};
		int[] runLengths = {5,2,4,3,5};
		
		RunLengthEncoding rle = new RunLengthEncoding(5, 5, 2, runs, runLengths);
		
		for (TypeAndSize run = rle.nextRun(); run != null; run = rle.nextRun() ) {
			System.out.println(toString.apply(run));
		}
	}
	
	private static void testNextRunWithRestart() {
		Function<Integer, String> typeStr = (i) -> {
			switch (i) {
			case Ocean.EMPTY:return "EMPTY";
			case Ocean.FISH:return "FISH";
			case Ocean.SHARK:return "SHARK";
			default: return "";
			}
		};
		
		Function<TypeAndSize, String> toString =
				(t) -> "TypeAndSize[type=" +
						typeStr.apply(t.type) + 
						", size=" +
						t.size + "]";
		int[] runs = {Ocean.SHARK,Ocean.FISH,Ocean.SHARK,Ocean.EMPTY,Ocean.FISH};
		int[] runLengths = {5,2,4,3,5};
		
		RunLengthEncoding rle = new RunLengthEncoding(5, 5, 2, runs, runLengths);
		
		for (TypeAndSize run = rle.nextRun(); run != null; run = rle.nextRun() ) {
			System.out.println(toString.apply(run));
		}
		System.out.println("restarting runs\n");
		
		rle.restartRuns();
		System.out.println(toString.apply(rle.nextRun()));
		System.out.println(toString.apply(rle.nextRun()));
		System.out.println("restarting runs\n");
		
		rle.restartRuns();
		for (TypeAndSize run = rle.nextRun(); run != null; run = rle.nextRun() ) {
			System.out.println(toString.apply(run));
		}
	}
	
	private static void testToOceanEmpty() {
		RunLengthEncoding rle = new RunLengthEncoding(5,5,5);
		SimText.paint(rle.toOcean());
	}
	
	private static void testToOcean5Empty5Sharks5Fish() {
		RunLengthEncoding rle = new RunLengthEncoding(
				3,
				5,
				2,
				new int[]{Ocean.EMPTY,Ocean.SHARK,Ocean.FISH},
				new int[]{5,5,5});
		Ocean sea = rle.toOcean();
		SimText.paint(sea);
	}
	
	private static void testOceanConstructorToOcean() {
		RunLengthEncoding rle = new RunLengthEncoding(
				3,
				5,
				2,
				new int[]{Ocean.EMPTY,Ocean.SHARK,Ocean.FISH},
				new int[]{5,5,5});
		
		System.out.println("These lists should be the same");
		System.out.println(rle.encoding.toString());
		rle = new RunLengthEncoding(rle.toOcean());
		
		System.out.println(rle.encoding.toString());
	}
	
	private static void testAddFish_0_0() {
		System.out.println("addFish(0, 0)");
		RunLengthEncoding rle = new RunLengthEncoding(5, 5, 2);
		System.out.println("start: " + rle.toString());
		rle.addFish(0, 0);
		System.out.println("expected: " 
				+ new RunLengthEncoding(
						5, 
						5, 
						2, 
						new int[]{Ocean.FISH, Ocean.EMPTY}, 
						new int[]{1,25}));
		System.out.println("end: " + rle.toString());
		System.out.println();
	}
	
	private static void testAddFish_4_4() {
		System.out.println("addFish(4, 4)");
		RunLengthEncoding rle = new RunLengthEncoding(5, 5, 2);
		System.out.println("start: " + rle.toString());
		rle.addFish(4, 4);
		System.out.println("expected: " 
				+ new RunLengthEncoding(
						5, 
						5, 
						2, 
						new int[]{Ocean.EMPTY, Ocean.FISH}, 
						new int[]{25, 1}));
		System.out.println("end: " + rle.toString());
		System.out.println();
	}
	
	private static void testAddFish_1_1() {
		System.out.println("addFish(1, 1)");
		RunLengthEncoding rle = new RunLengthEncoding(5, 5, 2);
		System.out.println("start: " + rle.toString());
		rle.addFish(1, 1);
		System.out.println("expected: " 
				+ new RunLengthEncoding(
						5, 
						5, 
						2, 
						new int[]{Ocean.EMPTY, Ocean.FISH, Ocean.EMPTY}, 
						new int[]{6, 1, 18}));
		System.out.println("end: " + rle.toString());
		System.out.println();
	}
	
	private static void testAddFish_5_0() {
		System.out.println("addFish(5, 0)");
		RunLengthEncoding rle = new RunLengthEncoding(
				5,
				5,
				2,
				new int[]{Ocean.SHARK, Ocean.EMPTY},
				new int[]{12,13});
		System.out.println("start: " + rle.toString());
		rle.addFish(5, 0);
		System.out.println("expected: " 
				+ new RunLengthEncoding(
						5, 
						5, 
						2, 
						new int[]{Ocean.SHARK, Ocean.FISH, Ocean.SHARK, Ocean.EMPTY}, 
						new int[]{5, 1, 6, 13}));
		System.out.println("end: " + rle.toString());
		System.out.println();
	}
	
	private static void testAddFish_0_4() {
		System.out.println("addFish(0, 4)");
		RunLengthEncoding rle = new RunLengthEncoding(
				5,
				5,
				2,
				new int[]{Ocean.SHARK, Ocean.EMPTY},
				new int[]{12,13});
		System.out.println("start: " + rle.toString());
		rle.addFish(0, 4);
		System.out.println("expected: " 
				+ new RunLengthEncoding(
						5, 
						5, 
						2, 
						new int[]{Ocean.SHARK, Ocean.EMPTY, Ocean.FISH, Ocean.EMPTY}, 
						new int[]{12, 8, 1, 4}));
		System.out.println("end: " + rle.toString());
		System.out.println();
	}
	
	private static void testAddFish_0_0_nextToFishRun() {
		System.out.println("addFish(0, 0)");
		RunLengthEncoding rle = new RunLengthEncoding(
				5,
				5,
				2,
				new int[]{Ocean.EMPTY, Ocean.FISH},
				new int[]{1,24});
		System.out.println("start: " + rle.toString());
		rle.addFish(0, 0);
		System.out.println("expected: " 
				+ new RunLengthEncoding(
						5, 
						5, 
						2, 
						new int[]{Ocean.FISH}, 
						new int[]{25}));
		System.out.println("end: " + rle.toString());
		System.out.println();
	}
	
	private static void testAddFish_4_4_nextToFishRun() {
		System.out.println("addFish(4, 4)");
		RunLengthEncoding rle = new RunLengthEncoding(
				5,
				5,
				2,
				new int[]{Ocean.FISH, Ocean.EMPTY},
				new int[]{24,1});
		System.out.println("start: " + rle.toString());
		rle.addFish(4, 4);
		System.out.println("expected: " 
				+ new RunLengthEncoding(
						5, 
						5, 
						2, 
						new int[]{Ocean.FISH}, 
						new int[]{25}));
		System.out.println("end: " + rle.toString());
		System.out.println();
	}
	
	private static void testAddFish_1_1_alreadyAFish() {
		System.out.println("addFish(1, 1)");
		RunLengthEncoding rle = new RunLengthEncoding(
				5,
				5,
				2,
				new int[]{Ocean.FISH},
				new int[]{25});
		System.out.println("start: " + rle.toString());
		rle.addFish(4, 4);
		System.out.println("expected: " 
				+ new RunLengthEncoding(
						5, 
						5, 
						2, 
						new int[]{Ocean.FISH}, 
						new int[]{25}));
		System.out.println("end: " + rle.toString());
		System.out.println();
	}
	
	private static void testGetElementPosition() {
		RunLengthEncoding rle = new RunLengthEncoding(5, 5, 2);
		
		for (int y = 0; y < 10; ++y) {
			for (int x = 0; x < 10; ++x) {
				System.out.format("(%d, %d) %d%n", 
						x,
						y,
						rle.getElementPosition(x, y));
			}
		}
	}
	
	private SimpleDList<String> encodeRuns(int[] runs, int[] runLengths) {
		SimpleDList<String> list = new SimpleDList<>();
		
		for (int i = 0; i < runs.length; ++i) {
			list.addLast( encodeRun(runs[i], runLengths[i]) );
		}
		currentRun = list.head.next;
		
		return list;
	}
	
	/**
	 * Encodes run from  the object type (run) and length (runLength)
	 * to a String representation
	 * @param run the type of object to be encoded
	 * @param runLength the length of the run
	 * @return
	 */
	private String encodeRun(int run, int runLength) {
		return encodeRun(run, runLength, starveTime);
	}
	
	private String encodeRun(int run, int runLength, int feeding) {
		String result = "";
		
		switch (run) {
		case Ocean.FISH:
			result = "F" + runLength;
			break;
		case Ocean.SHARK:
			result = "S" + feeding + "," + runLength;
			break;
		default:
			result = "." + runLength;
		}
		
		return result;
	}
	
	/**
	 * returns the number of cells in this receiver
	 * @return
	 */
	private int numCells() {
		int result = 0;
		Pattern p = Pattern.compile("\\d*\\z");
		for (
				SimpleDList.SimpleDNode<String> node = encoding.head.next;
				node != encoding.head;
				node = node.next) {
			Matcher m = p.matcher(node.data);
			m.find();
			int num = Integer.parseInt(m.group());
			System.out.println(num);
			result += num;
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		return encoding.toString();
	}
}
