package project1;

import java.util.Arrays;

/* Ocean.java */

/**
 * The Ocean class defines an object that models an ocean full of sharks and
 * fish. Descriptions of the methods you must implement appear below. They
 * include a constructor of the form
 *
 * public Ocean(int i, int j, int starveTime);
 *
 * that creates an empty ocean having width i and height j, in which sharks
 * starve after starveTime timesteps.
 *
 * See the README file accompanying this project for additional details.
 */

public class Ocean {

	/**
	 * Do not rename these constants. WARNING: if you change the numbers, you
	 * will need to recompile Test4.java. Failure to do so will give you a very
	 * hard-to-find bug.
	 */

	public final static int EMPTY = 0;
	public final static int SHARK = 1;
	public final static int FISH = 2;

	/**
	 * Define any variables associated with an Ocean object here. These
	 * variables MUST be private.
	 */
	
	private abstract class Occupant {
		public abstract int getType();
		public abstract int getFeeding();
		public String toString() {
			StringBuilder sb = new StringBuilder();
			switch (getType()) {
			case FISH:
				sb.append("fish");
				break;
			case SHARK:
				sb.append("shark");
				break;
			default:
				sb.append("?");
			}
			sb	.append("[feeding=" + getFeeding() + ",")
				.append("type=" + getType() + "]");
			
			return sb.toString();
		}
	}
	
	private class Fish extends Occupant {
		public int getType() {return FISH;}
		public int getFeeding() {return -1;}
	}
	
	private class Shark extends Occupant {
		private final int feeding;
		public Shark() {feeding = starveTime();}
		public Shark(int feeding) {this.feeding = feeding;}
		public int getType() {return SHARK;}
		public int getFeeding() {return feeding;}
	}
	
	private final int columns;
	private final int rows;
	private final Occupant[][] occupants;
	private final int starveTime;

	/**
	 * The following methods are required for Part I.
	 */

	/**
	 * Ocean() is a constructor that creates an empty ocean having width i and
	 * height j, in which sharks starve after starveTime timesteps.
	 * 
	 * @param i is the width of the ocean.
	 * @param j is the height of the ocean.
	 * @param starveTime
	 * 		is the number of timesteps sharks survive without food.
	 */

	public Ocean(int i, int j, int starveTime) {
		columns = i;
		rows = j;
		occupants = new Occupant[rows][columns];
		this.starveTime = starveTime;
	}

	/**
	 * width() returns the width of an Ocean object.
	 * 
	 * @return the width of the ocean.
	 */

	public int width() {
		// Replace the following line with your solution.
		return columns;
	}

	/**
	 * height() returns the height of an Ocean object.
	 * 
	 * @return the height of the ocean.
	 */

	public int height() {
		// Replace the following line with your solution.
		return rows;
	}

	/**
	 * starveTime() returns the number of timesteps sharks survive without food.
	 * @return the number of timesteps sharks survive without food.
	 */

	public int starveTime() {
		// Replace the following line with your solution.
		return starveTime;
	}

	/**
	 * addFish() places a fish in cell (x, y) if the cell is empty. If the cell
	 * is already occupied, leave the cell as it is.
	 * @param x is the x-coordinate of the cell to place a fish in.
	 * @param y is the y-coordinate of the cell to place a fish in.
	 */

	public void addFish(int x, int y) {
		occupants[ getPosY(y) ][ getPosX(x) ] = new Fish();
	}

	/**
	 * addShark() (with two parameters) places a newborn shark in cell (x, y) if
	 * the cell is empty. A "newborn" shark is equivalent to a shark that has
	 * just eaten. If the cell is already occupied, leave the cell as it is.
	 * 
	 * @param x
	 *            is the x-coordinate of the cell to place a shark in.
	 * @param y
	 *            is the y-coordinate of the cell to place a shark in.
	 */

	public void addShark(int x, int y) {
		occupants[ getPosY(y) ][ getPosX(x) ] = new Shark();
	}

	/**
	 * cellContents() returns EMPTY if cell (x, y) is empty, FISH if it contains
	 * a fish, and SHARK if it contains a shark.
	 * @param x is the x-coordinate of the cell whose contents are queried.
	 * @param y is the y-coordinate of the cell whose contents are queried.
	 */

	public int cellContents(int x, int y) {
		int posX = getPosX(x);
		int posY = getPosY(y);
		
		if (occupants[posY][posX] instanceof Shark) {
			return SHARK;
		}
		if (occupants[posY][posX] instanceof Fish) {
			return FISH;
		}
		return EMPTY;
	}

	/**
	 * timeStep() performs a simulation timestep as described in README.
	 * @return an ocean representing the elapse of one timestep.
	 */

	public Ocean timeStep() {
		Ocean next = new Ocean(columns, rows, starveTime + 1);
		
		for (int y = 0; y < rows; ++y) {
			for (int x = 0; x < columns; ++x) {
				next.setOccupant(x, y, getNextOccupant(x, y));
			}
		}
		
		return next;
	}

	/**
	 * The following method is required for Part II.
	 */

	/**
	 * addShark() (with three parameters) places a shark in cell (x, y) if the
	 * cell is empty. The shark's hunger is represented by the third parameter.
	 * If the cell is already occupied, leave the cell as it is. You will need
	 * this method to help convert run-length encodings to Oceans.
	 * @param x is the x-coordinate of the cell to place a shark in.
	 * @param y is the y-coordinate of the cell to place a shark in.
	 * @param feeding is an integer that indicates the shark's hunger. You may
	 *        encode it any way you want; for instance, "feeding" may be the
	 *        last timestep the shark was fed, or the amount of time that
	 *        has passed since the shark was last fed, or the amount of time
	 *        left before the shark will starve. It's up to you, but be
	 *        consistent.
	 */

	public void addShark(int x, int y, int feeding) {
		setOccupant( x, y, new Shark(feeding) );
	}

	/**
	 * The following method is required for Part III.
	 */

	/**
	 * sharkFeeding() returns an integer that indicates the hunger of the shark
	 * in cell (x, y), using the same "feeding" representation as the parameter
	 * to addShark() described above. If cell (x, y) does not contain a shark,
	 * then its return value is undefined--that is, anything you want. Normally,
	 * this method should not be called if cell (x, y) does not contain a shark.
	 * You will need this method to help convert Oceans to run-length encodings.
	 * 
	 * @param x is the x-coordinate of the cell whose contents are queried.
	 * @param y is the y-coordinate of the cell whose contents are queried.
	 */

	public int sharkFeeding(int x, int y) {
		Occupant o;
		
		if ( (o = getOccupant(x, y)) != null ) {
			return o.getFeeding();
		}
		
		return -1;
	}
	
	private int getPosX(int x) {
		return getAbsPosition(x, columns);
	}
	
	private int getPosY(int y) {
		return getAbsPosition(y, rows);
	}
	
	private static int getAbsPosition(int coord, int size) {
		if (coord < 0) {
			return Math.abs( (coord % size) + size) % size;
		}
		
		return coord % size;
	}
	
	private Occupant getNextOccupant(int x, int y) {
		Occupant result = null;
		Occupant current = getOccupant(x, y);
		
		if (current instanceof Shark) {
			result = getNextShark(x, y);
		}
		if (current instanceof Fish) {
			result = getNextFish(x, y);
		}
		if (current == null) {
			result = getNextEmpty(x, y);
		}
		
		return result;
	}
	
	private Occupant getNextShark(int x, int y) {
		Occupant current = getOccupant(x, y);
		Occupant result = null;
		Occupant[] neighbors = getNeighbors(x, y);
		
		boolean foundFish = false;
		for (Occupant neighbor : neighbors) {
			if (neighbor instanceof Fish) {
				foundFish = true;
				break;
			}
		}
		
		if ( current.getFeeding() > 0 ) {
			result = 
					foundFish ? 
					new Shark() : 
					new Shark(current.getFeeding() - 1);
		}
		
		return result;
	}
	
	private Occupant getNextFish(int x, int y) {
		Occupant result = null;
		Occupant[] neighbors = getNeighbors(x, y);
		
		int sharksFound = 0;
		for (Occupant neighbor : neighbors) {
			if (neighbor instanceof Shark) {
				++sharksFound;
			}
		}
		
		if (sharksFound > 1) {
			result = new Shark();
		} else if (sharksFound < 1) {
			result = new Fish();
		}
		
		return result;
	}
	
	private Occupant getNextEmpty(int x, int y) {
		Occupant result = null;
		Occupant[] neighbors = getNeighbors(x, y);
		
		int sharksFound = 0;
		int fishFound = 0;
		for (Occupant neighbor : neighbors) {
			if (neighbor instanceof Shark) {
				++sharksFound;
			}
			if (neighbor instanceof Fish) {
				++fishFound;
			}
		}
		
		if ( fishFound > 1 ) {
			result = sharksFound > 1 ? new Shark() : new Fish();
		}
		
		return result;
	}
	
	private Occupant[] getNeighbors(int x, int y) {
		Occupant[] neighbors = new Occupant[8];
		neighbors[0] = getOccupant(x - 1, y - 1	);
		neighbors[1] = getOccupant(x	, y - 1	);
		neighbors[2] = getOccupant(x + 1, y - 1	);
		neighbors[3] = getOccupant(x - 1, y		);
		neighbors[4] = getOccupant(x + 1, y		);
		neighbors[5] = getOccupant(x - 1, y + 1	);
		neighbors[6] = getOccupant(x	, y + 1	);
		neighbors[7] = getOccupant(x + 1, y + 1	);
		
		return neighbors;
	}
	
	private Occupant getOccupant(int x, int y) {
		return occupants[ getPosY(y) ][ getPosX(x) ];
	}
	
	private void setOccupant(int x, int y, Occupant occupant) {
		occupants[ getPosY(y) ][ getPosX(x) ] = occupant;
	}
	
//	test cases
	
//	Cell object consolidates access to a single cell
	private static class Cell {
		int x,y;
		Cell(int x_, int y_) {
			x = x_;
			y = y_;
		}
		public String toString() {return "" + x + "," + y;}
	}
	
	private static Ocean timeStepAndDisplay(Ocean sea, Cell[] toDisplay) {
		Ocean result = sea.timeStep();
		
		SimText.paint(result);
		for (Cell cell : toDisplay) {
			System.out.println(cell + " " + result.getOccupant(cell.x, cell.y));
		}
		System.out.println();
		
		return result;
	}
	
	private static void display(Ocean sea, Cell[] toDisplay) {
		SimText.paint(sea);
		for (Cell cell : toDisplay) {
			System.out.println(cell + " " + sea.getOccupant(cell.x, cell.y));
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
//		testSharkDeath();
//		testSharkEats();
//		testFishLife();
//		testSharksWithNoFood();
		testEmptyCellGetsNewShark();
	}
	
	private static void testSharkDeath() {
		Ocean sea = new Ocean(6,6,1);
		Cell[] sharkCell = {new Cell(0,0)};
		sea.addShark(sharkCell[0].x, sharkCell[0].y);
		
		display(sea, sharkCell);
		sea = timeStepAndDisplay(sea, sharkCell);
		timeStepAndDisplay(sea, sharkCell);
	}
	
	private static void testSharkEats() {
		Ocean sea = new Ocean(4,4,1);
		Cell[] cells = {new Cell(0,0), new Cell(-1,-1)};
		
		sea.addShark(cells[0].x, cells[0].y);
		sea.addFish(cells[1].x, cells[1].y);
		display(sea, cells);
		
		sea = timeStepAndDisplay(sea, cells);
		sea = timeStepAndDisplay(sea, cells);
		timeStepAndDisplay(sea, cells);
	}
	
	private static void testFishLife() {
		int i = 5, j = 5;
		Ocean sea = new Ocean(i,j,0);
		Cell[] cells = {
				new Cell(0,0),
				new Cell(i - 1, j - 1),
				new Cell(i / 2, j / 2)
		};
		
		for (Cell c : cells) {
			sea.addFish(c.x, c.y);
		}
		cells = Arrays.copyOf(cells, 4);
		cells[3] = new Cell(0, j / 2);
		
		display(sea, cells);
		sea = timeStepAndDisplay(sea, cells);
		sea = timeStepAndDisplay(sea, cells);
		timeStepAndDisplay(sea, cells);
	}
	
	private static void testSharksWithNoFood() {
		int i = 5, j = 5;
		Ocean sea = new Ocean(i,j,1);
		Cell[] cells = { new Cell(0,1), new Cell(1,1) };
		sea.addShark(cells[0].x, cells[0].y);
		sea.addShark(cells[1].x, cells[1].y);
		
		display(sea, cells);
		sea = timeStepAndDisplay(sea, cells);
		sea = timeStepAndDisplay(sea, cells);
	}
	
	private static void testEmptyCellGetsNewShark() {
		int i = 5, j = 5;
		Ocean sea = new Ocean(i,j,1);
		Cell shark1 = new Cell(1,1);
		Cell shark2 = new Cell(3,1);
		Cell empty = new Cell(2,1);
		Cell fish1 = new Cell(2,0);
		Cell fish2 = new Cell(2,2);
		
		sea.addShark(shark1.x, shark1.y);
		sea.addShark(shark2.x, shark2.y);
		sea.addFish(fish1.x, fish1.y);
		sea.addFish(fish2.x, fish2.y);
		
		Cell[] cells = {shark1,shark2,empty,fish1,fish2};
		display(sea,cells);
		timeStepAndDisplay(sea, cells);
	}
	
}
