package project1;

public class Sample {

	public static void main(String[] args) {
		test(3, -7, 2);
		test(3, -6, 0);
		test(3, -5, 1);
		test(3, -4, 2);
		test(3, -3, 0);
		test(3, -2, 1);
		test(3, -1, 2);
		test(3, 0, 0);
		test(3, 1, 1);
		test(3, 2, 2);
		test(3, 3, 0);
		test(3, 4, 1);
		test(3, 5, 2);
	}
	
	static void test(int size, int coord, int expected) {
		System.out.println(coord + " should be " + expected + ": " + getPos(coord, size));
	}
	
	static int getPos(int coord, int size) {
		return absCoordModSizePlusSizeModSize(coord, size);
	}
	
	static int sizeMinusAbsCoordModSize(int coord, int size) {
		if (coord < 0) {
			return size - (Math.abs(coord) % size);
		}
		
		return coord % size;
	}
	
	static int absCoordPlusSizeModSize(int coord, int size) {
		if (coord < 0) {
			return Math.abs(coord + size) % size;
		}
		
		return coord % size;
	}
	
	static int absCoordModSizePlusSizeModSize(int coord, int size) {
		if (coord < 0) {
			return Math.abs( (coord % size) + size) % size;
		}
		
		return coord % size;
	}

}
