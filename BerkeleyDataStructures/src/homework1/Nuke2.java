package homework1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Nuke2 {

	public static void main(String[] args) throws IOException {
		String inputLine = readString();
		System.out.println(inputLine.charAt(0) + inputLine.substring(2));
	}
	
	static String readString() throws IOException {
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		String inputLine = keyboard.readLine();
		keyboard.close();
		return inputLine;
	}
	
}
