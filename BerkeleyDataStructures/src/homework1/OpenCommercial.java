package homework1;

/* OpenCommercial.java */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Stack;

/**
 * A class that provides a main function to read five lines of a commercial Web
 * page and print them in reverse order, given the name of a company.
 */

class OpenCommercial {

	/**
	 * Prompts the user for the name X of a company (a single string), opens the
	 * Web site corresponding to www.X.com, and prints the first five lines of
	 * the Web page in reverse order.
	 * @param arg
	 * @exception Exception thrown if there are any problems parsing the user's input
	 * or opening the connection.
	 */
	public static void main(String[] arg) throws Exception {

		BufferedReader keyboard;
		String inputLine;

		keyboard = new BufferedReader(new InputStreamReader(System.in));

		System.out
				.print("Please enter the name of a company (without spaces): ");
		System.out.flush(); /* Make sure the line is printed immediately. */
		inputLine = keyboard.readLine();
		
		BufferedReader reader = new BufferedReader( 
				new InputStreamReader(
						new URL("http://www." + inputLine + ".com")
							.openConnection(
									new Proxy(Proxy.Type.HTTP, 
									new InetSocketAddress("nss", 8080)))
							.getInputStream() ) );
		Stack<String> lines = new Stack<>();
		int i = 0;
		
		String incoming;
		while ((incoming = reader.readLine()) != null && i++ < 5)
			lines.push(incoming);
		reader.close();
		while (!lines.isEmpty())
			System.out.println(lines.pop());
	}
}